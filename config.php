<?php 

/**
 * Database configuration.
 */

/**
 * Hostname.
 */
const DB_HOST = 'localhost';

/**
 * Database name.
 */
const DB_NAME = 'scandiweb';

/**
 * Database username.
 */
const DB_USER = 'root';

/**
 * Database password.
 */
const DB_PASS = '';

/**
 * DSN string for connecting with PDO.
 */
const DSN = 'mysql:host='. DB_HOST .';dbname='. DB_NAME .';charset=utf8mb4';

/**
 * PDO options.
 */
const DB_OPTIONS = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];

?>