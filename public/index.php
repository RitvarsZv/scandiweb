<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="/">
    <link rel="stylesheet" href="/css/main.css">
    <title>Scandiweb Test</title>
</head>
<body>
    <?php
        function autoloader($className) {
            include(__DIR__ . "\..\\" . $className . ".php");
        }
        spl_autoload_register("autoloader");

        require '../App/routes.php';
    ?>
</body>
</html>