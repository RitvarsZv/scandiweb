document.addEventListener("readystatechange", onTypeChange);
document.getElementById("type_id").addEventListener("change", onTypeChange);

//Show attribute fields based on selected type.
function onTypeChange() {
    hide();
    disableFields();
    let type = document.getElementById("type_id");
    let selectedType = type.options[type.selectedIndex];

    //Show attributes for selected type.
    document.getElementById(selectedType.value).style.display = 'block';
    let selectedAttributes = document.getElementById(selectedType.value);
    for (let e of selectedAttributes.getElementsByTagName('input')) {
        e.disabled = false;
    }
}

//Hides all attribute fields.
function hide() {
    let elems = document.getElementsByClassName("attributes");
    for (let e of elems) {
        e.style.display = 'none';
    } 
}

//Disable all attribute fields.
function disableFields() {
    let elems = document.getElementsByClassName("attributes");
    for (let e of elems) {
        let inputs = e.getElementsByTagName('input');
        for (let i of inputs) {
            i.disabled = true;
        }
    }
}