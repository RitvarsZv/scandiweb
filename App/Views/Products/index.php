<?php include '../App/Views/Templates/header.php' ?>

<div class="container">
    <form action="/mass-action" method="post">
        <div class="form-header">
            <h1 class="title">All products:</h1>
            <div class="actions">
                <select name="action">
                    <option value="delete">Mass delete</option>
                </select>
                <input type="submit" value="Apply">
            </div>
        </div>
        <div class="products">
            <?php foreach ($products as $p) { ?>
                <div class="product">
                    <input type="checkbox" class="float-left" name="products[<?= $p->getSku() ?>]">
                    <p><?= $p->getSku() ?></p>
                    <p><?= $p->getName() ?></p>
                    <p><?= $p->getPrice() ?> $</p>
                    <?php foreach ($p->getAttributes() as $a) { ?>
                        <p><?= $a->getName() ?>: <?= $a->getValue() .' '. $a->getMeasurement() ?></p>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </form>
</div>
<?php include '../App/Views/Templates/footer.php' ?>