<?php include '../App/Views/Templates/header.php' ?>

<div class="container">
    <form action="/add" method="post">
        <div class="form-header">
            <h1>Add a product:</h1>
            <input type="submit" class="float-right" value="Add product">
        </div>

        <?php if (isset($_REQUEST['error'])) { ?>
        <div class="error">
            <p>Ooops: <?= $_REQUEST['error'] ?></p>
        </div>
        <?php } ?>

        <div class="form-group">
            <label for="sku">SKU:</label>
            <input type="text" name="sku" id="sku" value="<?= $_REQUEST['sku']??'' ?>" required>
        </div>
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" name="name" id="name" value="<?= $_REQUEST['name']??'' ?>" required>
        </div>
        <div class="form-group">
            <label for="price">Price:</label>
            <input type="number" min="0" step="0.01" name="price" id="price" value="<?= $_REQUEST['price']??'' ?>" required>
        </div>
        <div class="form-group">
            <label for="type_id">Type:</label>
            <select name="type_id" id="type_id">
                <?php foreach ($types as $t) { ?>
                    <!-- I'm sorry for this. -->
                    <option value="<?= $t->getId() ?>" <?= (isset($_REQUEST['type_id'])&&$t->getId()==$_REQUEST['type_id'])?"selected":"" ?>><?= $t->getName() ?></option>
                <?php } ?>
            </select>
        </div>
        <hr>
        <?php foreach ($types as $t) { ?>
        <div class="attributes" id="<?= $t->getId() ?>">
            <?php foreach ($t->getAttributes() as $a) { ?>
            <div class="form-group">
                <label for="<?= $a->getId() ?>"><?= $a->getName() ?>:</label>
                <input  type="text" 
                        name="attributes[<?= $a->getId() ?>]" 
                        id="<?= $a->getId() ?>"
                        value="<?= $_REQUEST['attributes'][$a->getId()]??'' ?>" 
                        required>
                <p><?= $a->getDescription() ?></p>
            </div>
            <?php } ?>
        </div>
        <?php } ?>
        
    </form>
</div>

<?php include '../App/Views/Templates/footer.php' ?>
<script src="/js/add.js"></script>