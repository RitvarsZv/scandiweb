<?php

namespace App;

use App\Controllers\ProductController;

$productController = new ProductController();

$url = explode('?', $_SERVER['REQUEST_URI'], 2);
$method = $_SERVER['REQUEST_METHOD'];

if ($method == "GET") {
    switch ($url[0]) {
        case '/':
            $productController->index();
            break;
        case '/add':
            $productController->create();
            break;
        default:
            $productController->index();
            break;
    }
}

if ($method == "POST") {

    $params = $_POST;

    switch ($url[0]) {            
        case '/add':
            $productController->store($params);
            break;
        case '/mass-action':
            if ($params['action'] == 'delete' && isset($params['products'])) {
                $productController->delete($params['products']);
                break;
            }
            header('Location: /');
            break;
        default:
            $productController->index();
            break;
    }
}