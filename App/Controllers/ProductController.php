<?php

namespace App\Controllers;

use App\Models\Database;
use App\Models\ProductForm;
use App\Models\Product;
use App\Models\Attribute;
use Exception;

class ProductController
{

    private $dbc;

    public function __construct() {
        $this->dbc = new Database();
    }

    /**
     * Show a list of all products.
     * 
     * @return Void
     */
    public function index() {
        $products = $this->dbc->getAllProducts();
        require __DIR__.'/../Views/Products/index.php';
        die();    
    }

    /**
     * Show product add form.
     * 
     * @return Void
     */
    public function create() {
        $types = $this->dbc->getAllTypes();
        require __DIR__.'/../Views/Products/create.php';
        die();
    }

    /**
     * Save a product to database.
     * 
     * @param array $product
     * @return Void
     */
    public function store(array $product) {
        //Field validation.
        $form = new ProductForm($product['sku'], $product['name'], $product['price'], $product['type_id'], $product['attributes']);
        try {
            $form->validate();
        } catch (Exception $e) {
            $_REQUEST['error'] = $e->getMessage();
            $this->create();
        }

        //Insert product and attributes into db.
        $product = new Product($form->sku, $form->name, $form->price, $form->type_id);
        $this->dbc->insertProduct($product);
        foreach ($form->attributes as $id => $value) {
            $attribute = new Attribute();
            $attribute->setId($id);
            $attribute->setValue($value);
            $this->dbc->insertProductAttribute($product, $attribute);
        }
        header('Location: /');
    }

    /**
     * Delete given products.
     * 
     * @param array $products
     * @return Void
     */
    public function delete(array $products) {
        foreach ($products as $key => $value) {
            if ($this->dbc->productExists($key)) {
                $this->dbc->deleteProduct($key);
            }
        }
        header('Location: /');
    }
}