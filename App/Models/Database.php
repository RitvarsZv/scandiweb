<?php

namespace App\Models;

require __DIR__.'/../../config.php';
use App\Models\Product;
use App\Models\Type;
use App\Models\Attribute;
use PDO;
use PDOException;

class Database
{

    protected $dbc;

    public function __construct() {
        try {
            $this->dbc = new PDO(DSN, DB_USER, DB_PASS, DB_OPTIONS);
        } catch (PDOException $e) {
            die("Error!: " . $e->getMessage());
        }
    }

    /**
     * Function for PDO so getAllProducts() can make new Product objects with fetched rows.
     * Note: PDO's FETCH_CLASS doesn't give params to the constructor, so we use this workaround.
     * @param string $sku
     * @param string $name
     * @param float $price
     * @param int $type_id
     * @return Product
     */
    public function productMapper(string $sku, string $name, float $price, int $type_id) {
        $product = new Product($sku, $name, $price, $type_id);
        $product->attributes = $this->getProductAttributes($product);
        return $product;
    }

    /**
     * Returns an array of all products.
     * @return array(Product)
     */
    public function getAllProducts() {
        $query = $this->dbc->query('SELECT * FROM `products`');
        return $query->fetchAll(PDO::FETCH_FUNC, array($this, 'productMapper'));
    }

    /**
     * Returns an array of all attributes for a specific product.
     * @param Product $product
     * @return array(Attribute)
     */
    public function getProductAttributes(Product $product) {
        $query = $this->dbc->prepare(
            'SELECT attributes.id, attribute_name as name, measurement, value FROM product_attributes
            JOIN attributes ON attributes.id = product_attributes.attribute_id 
            WHERE product_attributes.product_sku = ?'
        );
        $query->execute(array($product->getSku()));
        return $query->fetchAll(PDO::FETCH_CLASS, 'App\Models\Attribute');
    }

    /**
     * Returns an array of all product types.
     * @return array(Type)
     */
    public function getAllTypes() {
        $query = $this->dbc->query('SELECT * FROM types');
        $types = $query->fetchAll(PDO::FETCH_CLASS, 'App\Models\Type');
        foreach ($types as $type) {
            $type->setAttributes($this->getTypeAttributes($type));
        }
        return $types;
    }

    /**
     * Returns an array of attributes for a specific type.
     * @param Type $type
     * @return array(Attribute)
     */
    public function getTypeAttributes(Type $type) {
        $query = $this->dbc->prepare(
            'SELECT attributes.id, attribute_name as name, measurement, description FROM `type_attributes` 
            JOIN attributes ON type_attributes.attribute_id = attributes.id
            WHERE type_id = ?'
        );
        $query->execute(array($type->getId()));
        return $query->fetchAll(PDO::FETCH_CLASS, 'App\Models\Attribute');
    }

    /**
     * Inserts a product into the database.
     * @param Product $product
     * @return Void
     */
    public function insertProduct(Product $product) {
        $query = $this->dbc->prepare('INSERT INTO products VALUES (?, ?, ?, ?)');
        $query->execute(array($product->getSku(), $product->getName(), $product->getPrice(), $product->getTypeId()));
    }

    /**
     * Deletes a product from the database.
     * @param string $sku
     * @return Void
     */
    public function deleteProduct(string $sku) {
        $query = $this->dbc->prepare('DELETE FROM products WHERE sku = ?');
        $query->execute(array(htmlspecialchars($sku)));
    }

    /**
     * Inserts a product attribute into the database.
     * @param Product $product
     * @param Attribute $attribute
     * @return Void
     */
    public function insertProductAttribute(Product $product, Attribute $attribute) {
        $query = $this->dbc->prepare('INSERT INTO product_attributes VALUES (NULL, ?, ?, ?)');
        $query->execute(array($attribute->getId(), $product->getSku(), $attribute->getValue()));
    }

    /**
     * Checks if a product with given sku exists in the database.
     * @param string $sku
     * @return bool
     */
    public function productExists(string $sku) {
        $query = $this->dbc->prepare('SELECT sku FROM products WHERE sku = ?');
        $query->execute(array(htmlspecialchars($sku)));
        return (bool)$query->rowCount();
    }

    /**
     * Checks if a type with given id exists in the database.
     * @param int $id
     * @return bool
     */
    public function typeExists(int $id) {
        $query = $this->dbc->prepare('SELECT id FROM types WHERE id = ?');
        $query->execute(array($id));
        return (bool)$query->rowCount();
    }

    /**
     * Checks if an attribute with given id exists in the database.
     * @param int $id
     * @return bool
     */
    public function attributeExists(int $id) {
        $query = $this->dbc->prepare('SELECT id FROM attributes WHERE id = ?');
        $query->execute(array($id));
        return (bool)$query->rowCount();
    } 
}
