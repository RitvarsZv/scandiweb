<?php

namespace App\Models;

use App\Models\Database;
use Exception;

class ProductForm
{

    public $sku; 
    public $name;
    public $price;
    public $type_id;
    public $attributes;

    public function __construct($sku, $name, $price, $type_id, $attributes) {
        $this->sku = htmlspecialchars(trim($sku));
        $this->name = htmlspecialchars(trim($name));
        $this->price = htmlspecialchars(trim($price));
        $this->type_id = htmlspecialchars(trim($type_id));
        $this->attributes = $attributes;
    }

    /**
     * Validates all object attributes.
     * @return Void
     */
    public function validate() {
        $errors = [];
        $message = "";

        if (empty($this->sku)) 
            array_push($errors, "SKU");
        if (empty($this->name))
            array_push($errors, "Name");
        if (empty($this->price))
            array_push($errors, "Price");
        if (empty($this->type_id))
            array_push($errors, "Type");
        foreach ($this->attributes as $key => $value) {
            if (empty(trim($value))) {
                array_push($errors, "Attributes");
                break;
            }
        }
        if (!empty($errors)) {
            foreach ($errors as $e) {
                $message .= $e.", ";
            } 
            $message .= "cannot be empty!";
            throw new Exception($message);
        }

        if (!is_numeric($this->price))
            throw new Exception("Price must be a number!");
        if ($this->price < 0) 
            throw new Exception("Price must be a positive number!");

        $db = new Database();

        if ($db->productExists($this->sku)) {
            throw new Exception("Product with that SKU already exists!");
        }

        if (!$db->typeExists($this->type_id)) {
            throw new Exception("It seems that the selected product type doesn't exist!");
        }

        foreach ($this->attributes as $id => $value) {
            $this->attributes[$id] = htmlspecialchars(trim($value));
            if (!is_numeric($id) || !$db->attributeExists($id)) {
                throw new Exception("It seems that something wen't wrong with product attributes!");
            }
        }

    }
}