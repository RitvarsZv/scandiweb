<?php

namespace App\Models;

class Product
{
    protected string $sku;
    protected string $name;
    protected float $price;
    protected int $type_id;
    public array $attributes; 

    public function __construct(string $sku, string $name, float $price, int $type_id) {
        $this->setSku(trim($sku));
        $this->setName(trim($name));
        $this->setPrice(trim($price));
        $this->setTypeId(trim($type_id));
    }

    public function getSku() {
        return $this->sku;
    }

    public function setSku(string $sku) {
        $this->sku = htmlspecialchars($sku);
    }

    public function getName() {
        return $this->name;
    }

    public function setName(string $name) {
        $this->name = htmlspecialchars($name);
    }

    public function getPrice() {
        return number_format($this->price, 2);
    }

    public function setPrice(float $price) {
        $this->price = number_format($price, 2);
    }

    public function getTypeId() {
        return $this->type_id;
    }

    public function setTypeId(int $type_id) {
        $this->type_id = $type_id;
    }

    public function setAttributes(array $attributes) {
        $this->attributes = $attributes;
    }

    public function getAttributes() {
        return $this->attributes;
    }
}
