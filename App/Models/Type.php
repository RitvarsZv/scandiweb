<?php

namespace App\Models;

class Type
{
    protected int $id;
    protected string $type_name;
    protected array $attributes;

    public function getId() {
        return $this->id;
    }

    public function setId(int $id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->type_name;
    }

    public function setName(string $name) {
        $this->type_name = $name;
    }

    public function getAttributes() {
        return $this->attributes;
    }

    public function setAttributes(array $attributes) {
        $this->attributes = $attributes;
    }
}
