<?php

namespace App\Models;

class Attribute
{
    protected int $id;
    protected string $name;
    protected ?string $measurement;
    protected ?string $description;
    protected string $value;

    public function getId() {
        return $this->id;
    }

    public function setId(int $id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName(string $name) {
        $this->name = $name;
    }

    public function getMeasurement() {
        return $this->measurement;
    }

    public function setMeasurement(string $measurement) {
        $this->measurement = $measurement;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription(string $description) {
        $this->description = $description;
    }

    public function getValue() {
        return $this->value;
    }

    public function setValue(string $value) {
        $this->value = $value;
    }

    
}
